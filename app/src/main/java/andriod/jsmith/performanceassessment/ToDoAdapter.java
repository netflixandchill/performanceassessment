package andriod.jsmith.performanceassessment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class ToDoAdapter extends RecyclerView.Adapter<ToDoHolder> {

    private ArrayList<ToDoPost> todoPosts;
    private ActivityCallback activityCallback;
    private ToDoHolder holder;

    public ToDoAdapter(ActivityCallback activityCallback, ArrayList<ToDoPost> todoPosts)  {
        this.activityCallback = activityCallback;
        this.todoPosts = todoPosts;
    }

    public ArrayList<ToDoPost> gettodoPosts() {
        return todoPosts;
    }

    @Override
    public ToDoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new ToDoHolder(view);
    }

    @Override
    public void onBindViewHolder(ToDoHolder holder, final int i) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityCallback.onPostSelected(i);
            }
        });
        holder.titleText.setText(todoPosts.get(i).title);
    }

    @Override
    public int getItemCount() {
        return todoPosts.size();
    }
}
