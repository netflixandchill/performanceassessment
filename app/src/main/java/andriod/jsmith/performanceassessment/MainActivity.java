package andriod.jsmith.performanceassessment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;

public class MainActivity extends SingleFragmentActivity implements ActivityCallback{
    public ArrayList<ToDoPost> todoPosts = new ArrayList<ToDoPost>();
    public int currentItems;
    private ViewPager viewPager;

    @LayoutRes
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }

    @Override
    protected Fragment createFragment() {
        return new ToDoFragment();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
    }

    @Override
    public void onPostSelected(int pos) {
       currentItems = pos;
       Fragment newFragment = new ItemFragment();

       FragmentTransaction transaction = getFragmentManager().beginTransaction();
       transaction.replace(R.id.fragment_container, newFragment);

       transaction.addToBackStack(null);
       transaction.commit();
   }
}
